interface Alimento {
}

interface Animal<T> {
    alimentar :(T:any) => void;
}

class Vegetal implements Alimento{
    
}

class Inseto implements Animal<Vegetal>{
    alimentar(alimento:Vegetal){
        console.log("!");
    }
}

class Anfibio implements Animal<Inseto>{
    alimentar(alimento:Vegetal){
        console.log("!");
    }
}

class Decompositor implements Animal<Alimento>{
    alimentar(alimento:Alimento){
        console.log("!");
    }
}
