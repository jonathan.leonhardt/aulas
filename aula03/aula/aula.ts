enum cores{azul = 'azul',vermelho='vermelho',verde='verde',amarelo='amarelo',laranja='laranja'};

interface Roupa {
    usando: boolean;
    setUsar: () => void;
    isUsando: () => boolean;
}

interface Material {
    materiaPrima: string;
    cor:cores;
    tipo: string;
    getMaterial: () => string;
    setMaterial: (materiaPrima: string) => void;
    getTipo: () => string;
    setTipo: (tipo: string) => void;
}

class Blusao implements Roupa, Material {
    cor: cores;
    materiaPrima: string;
    usando: boolean;
    tipo: string;
    constructor(cor: cores, materiaPrima: string, usando: boolean, tipo: string) {
        this.cor = cor;
        this.materiaPrima = materiaPrima;
        this.usando = usando;
        this.tipo = tipo;
    }
    setUsar() {
        if (this.usando) {
            this.usando = false;
            console.log('blusão não está sendo utilizado')
        } else {
            this.usando = true
            console.log('blusão está sendo utilizado')
        }
    }
    isUsando() {
        return this.usando
    }
    getMaterial() {
        return this.materiaPrima;
    }
    setMaterial(novoMaterial: string) {
        this.materiaPrima = novoMaterial;
    }
    getTipo() {
        return this.tipo;
    }
    setTipo(novoTipos: string) {
        this.tipo = novoTipos;
    }
}

class Camiseta implements Roupa, Material {
    cor: cores;
    materiaPrima: string;
    usando: boolean;
    tipo: string;
    constructor(cor: cores, materiaPrima: string, usando: boolean, tipo: string) {
        this.cor = cor;
        this.materiaPrima = materiaPrima;
        this.usando = usando;
        this.tipo = tipo;
    }
    setUsar() {
        if (this.usando) {
            this.usando = false;
            console.log('blusão não está sendo utilizado')
        } else {
            this.usando = true
            console.log('blusão está sendo utilizado')
        }
    }
    isUsando() {
        return this.usando
    }
    getMaterial() {
        return this.materiaPrima;
    }
    setMaterial(novoMaterial: string) {
        this.materiaPrima = novoMaterial;
    }
    getTipo() {
        return this.tipo;
    }
    setTipo(novoTipos: string) {
        this.tipo = novoTipos;
    }
}
let roupas: Array<Roupa> = [new Blusao(cores.amarelo, 'algodao', false, 'manga longa'),
new Camiseta(cores.azul, 'tecido', false, 'manga curta'),
new Camiseta(cores.amarelo, 'tecido', false, 'manga curta', ),
new Blusao(cores.verde, 'algodao', true, 'manga longa'),
new Camiseta(cores.verde, 'algodao', true, 'manga curta')]

roupas.map(function (element) {
    console.log(element);
});
