"use strict";
var cores;
(function (cores) {
    cores["azul"] = "azul";
    cores["vermelho"] = "vermelho";
    cores["verde"] = "verde";
    cores["amarelo"] = "amarelo";
    cores["laranja"] = "laranja";
})(cores || (cores = {}));
;
class Blusao {
    constructor(cor, materiaPrima, usando, tipo) {
        this.cor = cor;
        this.materiaPrima = materiaPrima;
        this.usando = usando;
        this.tipo = tipo;
    }
    setUsar() {
        if (this.usando) {
            this.usando = false;
            console.log('blusão não está sendo utilizado');
        }
        else {
            this.usando = true;
            console.log('blusão está sendo utilizado');
        }
    }
    isUsando() {
        return this.usando;
    }
    getMaterial() {
        return this.materiaPrima;
    }
    setMaterial(novoMaterial) {
        this.materiaPrima = novoMaterial;
    }
    getTipo() {
        return this.tipo;
    }
    setTipo(novoTipos) {
        this.tipo = novoTipos;
    }
}
class Camiseta {
    constructor(cor, materiaPrima, usando, tipo) {
        this.cor = cor;
        this.materiaPrima = materiaPrima;
        this.usando = usando;
        this.tipo = tipo;
    }
    setUsar() {
        if (this.usando) {
            this.usando = false;
            console.log('blusão não está sendo utilizado');
        }
        else {
            this.usando = true;
            console.log('blusão está sendo utilizado');
        }
    }
    isUsando() {
        return this.usando;
    }
    getMaterial() {
        return this.materiaPrima;
    }
    setMaterial(novoMaterial) {
        this.materiaPrima = novoMaterial;
    }
    getTipo() {
        return this.tipo;
    }
    setTipo(novoTipos) {
        this.tipo = novoTipos;
    }
}
let roupas = [new Blusao(cores.amarelo, 'algodao', false, 'manga longa'),
    new Camiseta(cores.azul, 'tecido', false, 'manga curta'),
    new Camiseta(cores.amarelo, 'tecido', false, 'manga curta'),
    new Blusao(cores.verde, 'algodao', true, 'manga longa'),
    new Camiseta(cores.verde, 'algodao', true, 'manga curta')];
roupas.map(function (element) {
    console.log(element);
});
