var lista = [];
lista.push(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

function usandoFilter() {
    return lista.filter(function (param) {
        return param < 5;
    });
}

function usandoMap() {
    return lista.map(function (param) {
        /*console.log(param +' * 3 =' + param * 3);
        console.log('------------------------');*/
        return param * 3;
    });
}

function usandoFind() {
    return lista.find(function (param) {
        
        return param === 5;
    });
}

/*
function usandoFind() {
    return lista.find(function (param, index, array) {
        console.log(param, index, array);
        return param === 5;
    });
}*/