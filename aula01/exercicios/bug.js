function removeItens(lista, exclusoes) {
    for (var i = 0; i < lista.length; i++) {
        if (~exclusoes.indexOf(lista[i])) {
            lista.splice(i, 1)
        }
    }
    console.log(lista)
}

// CORREÇÃO
function removeItensCorreta(lista, exclusoes) {
    for (var i = 0; i < lista.length; i++) {
        if (~exclusoes.indexOf(lista[i])) {
            lista.splice(i, 1)
            i--;
        }
    }
    console.log(lista)
}

/* por que o problema acontece?
    - splice remove o  elemento do arrray, logo, se removermos o
      elemento  1 do array [1, 2, 3, 4, 5] iremos remover o index 0
      após removido o index 0 o FOR da função irá seguir para o
      próximo elemento que no nosso caso não será mais o 2
      pois agora o array é [2, 3, 4, 5]
*/

//TESTE
removeItensCorreta([1, 2, 3, 4, 5], [1, 2])
//removeItens([1, 2, 3, 4, 5], [1, 2])
removeItensCorreta([1, 2, 3, 4, 5], [1, 3])
//removeItens([1, 2, 3, 4, 5], [1, 3])
removeItensCorreta([1, 2, 3, 4, 5], [1, 4])
//removeItens([1, 2, 3, 4, 5], [1, 4])
removeItensCorreta([1, 2, 3, 4, 5], [1, 5])
//removeItens([1, 2, 3, 4, 5], [1, 5])
console.log('------------------------------');
removeItensCorreta([1, 2, 3, 4, 5], [2, 1])
//removeItens([1, 2, 3, 4, 5], [2, 1])
removeItensCorreta([1, 2, 3, 4, 5], [2, 3])
//removeItens([1, 2, 3, 4, 5], [2, 3])
removeItensCorreta([1, 2, 3, 4, 5], [2, 4])
//removeItens([1, 2, 3, 4, 5], [2, 4])
removeItensCorreta([1, 2, 3, 4, 5], [2, 5])
//removeItens([1, 2, 3, 4, 5], [2, 5])
console.log('------------------------------');
removeItensCorreta([1, 2, 3, 4, 5], [3, 2])
//removeItens([1, 2, 3, 4, 5], [3, 2])
removeItensCorreta([1, 2, 3, 4, 5], [3, 4])
//removeItens([1, 2, 3, 4, 5], [3, 4])
removeItensCorreta([1, 2, 3, 4, 5], [3, 5])
//removeItens([1, 2, 3, 4, 5], [3, 5])
removeItensCorreta([1, 2, 3, 4, 5], [3, 1])
//removeItens([1, 2, 3, 4, 5], [3, 1])
console.log('------------------------------');
removeItensCorreta([1, 2, 3, 4, 5], [4, 1])
//removeItens([1, 2, 3, 4, 5], [4, 1])
removeItensCorreta([1, 2, 3, 4, 5], [4, 2])
//removeItens([1, 2, 3, 4, 5], [4, 2])
removeItensCorreta([1, 2, 3, 4, 5], [4, 3])
//removeItens([1, 2, 3, 4, 5], [4, 3])
removeItensCorreta([1, 2, 3, 4, 5], [4, 5])
//removeItens([1, 2, 3, 4, 5], [4, 5])
console.log('------------------------------');
removeItensCorreta([1, 2, 3, 4, 5], [5, 1])
//removeItens([1, 2, 3, 4, 5], [5, 1])
removeItensCorreta([1, 2, 3, 4, 5], [5, 2])
//removeItens([1, 2, 3, 4, 5], [5, 2])
removeItensCorreta([1, 2, 3, 4, 5], [5, 3])
//removeItens([1, 2, 3, 4, 5], [5, 3])
removeItensCorreta([1, 2, 3, 4, 5], [5, 4])
//removeItens([1, 2, 3, 4, 5], [5, 4])