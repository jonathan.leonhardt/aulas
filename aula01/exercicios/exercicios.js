/*
Funções
*/

function multiplicar(param1, param2) {
  return para1*param2;
}

function subtrair(param1, param2) {
  return para1-param2;
}

function reverteString(string) {
  return string.split('').reverse().join('');
}

function removeVogaisString(string) {
  return string.replace(/[aeiou]/ig,'');
}

/*
Arrays
*/
function usandoFilter(array) {
  return array.filter(function (element){
    return element.length>10;
  });
}

function usandoMap(array) {
  return array.map(function(element) {
    return reverteString(element);
  })
}

function usandoSort (array)  {
  return array.sort(function (elemt1, elemt2){
      return elemt1.localeCompare(elemt2);
  });
}

//console.log(usandoSort(['sahusa shuahsuahu', 'aaaasahus', 'AAAAAA','aaaaa', 'C', 'ccc']))

function usandoReduce (array) {
  return array.reduce(function(total, element) {
    return total + element;
  });
}
