interface SerVivo {
    nome: string;
    idade: number | string;
    //id: number;
    //estaVivo: boolean;
    //vive: () => boolean;
}

interface Profissao {
    nomeProfissao: string;
    duracao: number;
}

class Profissional implements Profissao, SerVivo {
    static _estaVivo: boolean;
    //static id: number = 0;
    private _nome: string = 'Ser sem nome!';
    private _idade: number | string = 'Ser sem idade!';
    private _duracao: number;
    private _nomeProfissao: string = 'Sem profissão definida';

    constructor(nome?: string, idade?: number|string, nomeProfissao?: string) {
        //Profissional.id = Profissional.id+1;
        //this.estaVivo = true;
        this._duracao = 0;
        if (idade) this._idade = idade;
        if (nome) this._nome = nome;
        if (nomeProfissao) this._nomeProfissao = nomeProfissao;
    }

    public trabalhar(param?: string) {
        return 'O professional está trabalhando';
    }

    //public get estaVivo(): boolean {
    //return this.estaVivo;
    //}
    //public set estaVivo(v: boolean) {
    //this.estaVivo = v;
    //}
    public get idade(): number | string {
        return this._idade;
    }
    public set idade(v: number | string) {
        this._idade = v;
    }
    public get nome(): string {
        return this._nome;
    }
    public set nome(v: string) {
        this._nome = v;
    }
    public get id(): number {
        return this.id;
    }
    public get nomeProfissao(): string {
        return this._nomeProfissao;
    }
    public set nomeProfissao(v: string) {
        this._nomeProfissao = v;
    }
    public get duracao(): number {
        return this._duracao;
    }
    public set duracao(v: number) {
        this._duracao = v;
    }
}

class Engen extends Profissional {
    constructor(nome: string, idade: number | string) {
        super(nome,idade)
        this.nomeProfissao = 'Engenheiro';
    }
    trabalhar(tipoEngenharia?: string) {
        return tipoEngenharia ? 'O engenheiro está trabalhando com ' + tipoEngenharia + '!' : 'O engenheiro está trabalhando!'
    }
    constroiPredio(): string {
        if (this.trabalhar('Engenharia Civil'))
            return 'Predio construído pelo engenheiro ' + this.nome + '!';
        return 'O engenheiro não trabalhou com Engenharia Civil e o prédio não foi construído';
    }
}

class Coord extends Profissional {
    constructor(nome: string, idade: number | string) {
        super(nome,idade)
        this.nomeProfissao = 'Coordenador';
    }
    trabalhar(equipe?: string) {
        return equipe ? 'O coordenador está coordenando a equipe ' + equipe + '!' : 'O coordenador está trabalhando!'
    }
    lideraEquipe(nomeEquipe: string): string {
        this.trabalhar(nomeEquipe);
        return (Math.floor(Math.random() * (3)) + 0) % 2 === 0 ? nomeEquipe + ' liderada!' : 'Equipe não liderada! ;(';
    }
}

class Prof extends Profissional {
    constructor(nome: string, idade: number | string) {
        super(nome,idade)
        this.nomeProfissao = 'Porfessor';
    }
    trabalhar(turma?: string) {
        return turma ? 'O professor está coordenando a turma ' + turma + '!' : 'O professor está trabalhando!'
    }
    ensinarTurma(nomeTurma: string): string {
        this.trabalhar(nomeTurma)
        let quantAlunos = (Math.floor(Math.random() * (111)) + 1)
        return 'De ' + quantAlunos + ' apenas ' + (Math.floor(Math.random() * (quantAlunos)) + 1) + ' aprenderam!';
    }
    planejarAula(nomeTurma?: string): string {
        return nomeTurma ? 'Aula para turma ' + nomeTurma + ' sendo planejada!' : 'Professor planejando a aula!';
    }
}

class Jogad extends Profissional {
    private _lesionado: boolean;
    constructor(nome:string, idade:number|string){
        super(nome,idade)
        this.nomeProfissao = 'Jogador';
        this._lesionado=false;
    }
    trabalhar(time?: string) {
        return time ? 'O coordenador está jogando para o time ' + time + '!' : 'O coordenador está trabalhando!'
    }
    entrarCampo(nomeEstadio: string): string {
        return !this.lesionado ? nomeEstadio : 'Jogador lesionado';
    }
    jogarPartida(nomeTime: string, nomeEstadio: string): string {
        let podeJogar = this.entrarCampo(nomeEstadio);
        if (podeJogar === 'Jogador lesionado')
            return podeJogar
        else {
            this.trabalhar(nomeTime);
            return 'Partida terminou: ' + (Math.floor(Math.random() * (10))) + '' + (Math.floor(Math.random() * (10)));
        }
    }

    public get lesionado(): boolean {
        return this._lesionado;
    }
    public set lesionado(v: boolean) {
        this._lesionado = v;
    }
}



let engen = new Engen('Jonathan', 18)
let coord = new Coord('Jonathan', 18)
let prof = new Prof('Jonathan', 18)
let jogad = new Jogad('Jonatahn', 18)
let autonomo = new Profissional('Jonathan', 222, 'Autonomo')
let profissional = new Profissional('Jonathan', 18)
console.log(engen);
console.log(engen.constroiPredio());
console.log(coord);
console.log(coord.lideraEquipe('EQUIPE01'));
console.log(prof);
console.log(prof.planejarAula());
console.log(prof.ensinarTurma('TURMA01'));
console.log(jogad);
console.log(jogad.jogarPartida('TIME01', 'ESTADIO02'));
console.log(autonomo);
console.log(autonomo.trabalhar());
console.log(profissional);
console.log(profissional.trabalhar());


