"use strict";
class Profissional {
    constructor(nome, idade, nomeProfissao) {
        this._nomeProfissao = 'Sem profissão definida';
        //Profissional.id = Profissional.id+1;
        this.estaVivo = true;
        this._duracao = 0;
        this._nome = nome;
        this._idade = idade;
        if (nomeProfissao)
            this._nomeProfissao = nomeProfissao;
    }
    trabalhar(param) {
        return 'O professional está trabalhando';
    }
    vive() {
        return this.estaVivo;
    }
    get estaVivo() {
        return this.estaVivo;
    }
    set estaVivo(v) {
        this.estaVivo = v;
    }
    get idade() {
        return this._idade;
    }
    set idade(v) {
        this._idade = v;
    }
    get nome() {
        return this._nome;
    }
    set nome(v) {
        this._nome = v;
    }
    get id() {
        return this.id;
    }
    get nomeProfissao() {
        return this._nomeProfissao;
    }
    set nomeProfissao(v) {
        this._nomeProfissao = v;
    }
    get duracao() {
        return this._duracao;
    }
    set duracao(v) {
        this._duracao = v;
    }
}
class Engen extends Profissional {
    constructor() {
        super(...arguments);
        this.nomeProfissao = 'Engenheiro';
    }
    trabalhar(tipoEngenharia) {
        return tipoEngenharia ? 'O engenheiro está trabalhando com ' + tipoEngenharia + '!' : 'O engenheiro está trabalhando!';
    }
    constroiPredio() {
        if (this.trabalhar('Engenharia Civil'))
            return 'Predio construído pelo engenheiro ' + this.nome + '!';
        return 'O engenheiro não trabalhou com Engenharia Civil e o prédio não foi construído';
    }
}
class Coord extends Profissional {
    constructor() {
        super(...arguments);
        this.nomeProfissao = 'Coordenador';
    }
    trabalhar(equipe) {
        return equipe ? 'O coordenador está coordenando a equipe ' + equipe + '!' : 'O coordenador está trabalhando!';
    }
    lideraEquipe(nomeEquipe) {
        this.trabalhar(nomeEquipe);
        return (Math.floor(Math.random() * (3)) + 0) % 2 === 0 ? nomeEquipe + ' liderada!' : 'Equipe não liderada! ;(';
    }
}
class Prof extends Profissional {
    constructor() {
        super(...arguments);
        this.nomeProfissao = 'Professor';
    }
    trabalhar(turma) {
        return turma ? 'O professor está coordenando a turma ' + turma + '!' : 'O professor está trabalhando!';
    }
    ensinarTurma(nomeTurma) {
        this.trabalhar(nomeTurma);
        return 'De ' + (Math.floor(Math.random() * (111))) + ' apenas ' + (Math.floor(Math.random() * (100)) + 1) + ' aprenderam!';
    }
    planejarAula(nomeTurma) {
        return nomeTurma ? 'Aula para turma ' + nomeTurma + ' sendo planejada!' : 'Professor planejando a aula!';
    }
}
class Jogad extends Profissional {
    constructor() {
        super(...arguments);
        this.nomeProfissao = 'Jogador';
        this._lesionado = false;
    }
    trabalhar(time) {
        return time ? 'O coordenador está jogando para o time ' + time + '!' : 'O coordenador está trabalhando!';
    }
    entrarCampo(nomeEstadio) {
        return !this.lesionado ? nomeEstadio : 'Jogador lesionado';
    }
    jogarPartida(nomeTime, nomeEstadio) {
        let podeJogar = this.entrarCampo(nomeEstadio);
        if (podeJogar === 'Jogador lesionado')
            return podeJogar;
        else {
            this.trabalhar(nomeTime);
            return 'Partida terminou: ' + (Math.floor(Math.random() * (10))) + '' + (Math.floor(Math.random() * (10)));
        }
    }
    get lesionado() {
        return this._lesionado;
    }
    set lesionado(v) {
        this._lesionado = v;
    }
}
let engen = new Engen('Jonathan', 18);
let coord = new Coord('Jonathan', 18);
let prof = new Prof('Jonathan', 18);
let jogad = new Jogad('Jonatahn', 18);
let autonomo = new Profissional('Jonathan', 222, 'Autonomo');
let profissional = new Profissional('Jonathan', 18);
console.log(engen);
console.log(coord);
console.log(prof);
console.log(jogad);
console.log(autonomo);
console.log(profissional);
