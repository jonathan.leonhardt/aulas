"use strict";
class Coordenador {
    constructor(nome) {
        this._nome = nome;
    }
    trabalhar() {
        return 'coordenador esta trabalhando';
    }
    get nome() {
        return this.nome;
    }
    set nome(nome) {
        this.nome = nome;
    }
}
class Jogador {
    constructor(nome) {
        this._nome = nome;
    }
    get nome() {
        return this._nome;
    }
    set nome(v) {
        this._nome = v;
    }
    trabalhar() {
        return 'jogador esta trabalhando';
    }
}
class Professor {
    constructor(nome) {
        this._nome = nome;
    }
    get nome() {
        return this._nome;
    }
    set nome(v) {
        this._nome = v;
    }
    trabalhar() {
        return 'professor esta trabalhando';
    }
}
class Engenheiro {
    constructor(nome) {
        this._nome = nome;
    }
    get nome() {
        return this._nome;
    }
    set nome(v) {
        this._nome = v;
    }
    trabalhar() {
        return 'engenheiro esta trabalhando';
    }
}
let professor = new Professor('Joao');
let coordenador = new Coordenador('Pedro');
let engenheiro = new Engenheiro('Maria');
let jogador = new Jogador('Joana');
