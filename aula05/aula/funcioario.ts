interface funcionario{
    nome:string
    trabalhar: () =>string;
}
class Coordenador implements funcionario{
    private _nome:string;
    constructor(nome:string){
        this._nome = nome;
    }
    trabalhar(){
        return 'coordenador esta trabalhando';
    }
    get nome(){
        return this.nome
    }
    set nome(nome:string){
        this.nome = nome;
    }
}
class Jogador implements funcionario{
    private _nome : string;
    constructor(nome:string){
        this._nome = nome;
    }
    public get nome() : string {
        return this._nome;
    }
    public set nome(v : string) {
        this._nome = v;
    }
    trabalhar(){
        return 'jogador esta trabalhando';
    }
}
class Professor implements funcionario{
    private _nome : string;
    constructor(nome:string){
        this._nome = nome;
    }
    public get nome() : string {
        return this._nome;
    }
    public set nome(v : string) {
        this._nome = v;
    }
    trabalhar(){
        return 'professor esta trabalhando';
    }
}
class Engenheiro implements funcionario{
    private _nome : string;
    constructor(nome:string){
        this._nome = nome;
    }
    public get nome() : string {
        return this._nome;
    }
    public set nome(v : string) {
        this._nome = v;
    }
    trabalhar(){
        return 'engenheiro esta trabalhando';
    }
}

let professor = new Professor('Joao');
let coordenador = new Coordenador('Pedro')
let engenheiro = new Engenheiro('Maria')
let jogador = new Jogador('Joana')