/* usar ts-node */

//FUNÇÕES
function multiplica (param1:number, param2:number):number{
    return param1*param2;
}
console.log(`multiplicar: ${multiplica(2, 2)}`);

function subtraiv2 (param1:number, param2:number):number{
    return param1-param2;
}
console.log(`subtrair: ${subtraiv2(4, 2)}`);

function reverterString(str:string):string{
    return str.split('').reverse().join('');
}
console.log(`String Para ser Invertida: ${reverterString('revertida')}'`);

function removerVogais(str:string):string{
    return str.replace(/[aeiou]/ig,'');
}
console.log(`String Para Remover Vogais: ${removerVogais('vogais')}'`);


//ARRAYS
function usandoFilterComTS(arr:Array<string>):Array<string>{
    return arr.filter(function (element:string):boolean{
        return element.length>10;
    });
};
console.log(`Utilizando Filter: ${usandoFilterComTS(['vogais','vogal maior', 'vogal ainda maior'])}'`);

function usandoMapComTS(arr:Array<string>):Array<string>{
    return arr.map(function(element) {
        return reverterString(element);
    });
};
console.log(`Utilizando Map: ${usandoMapComTS(['vogais','vogal maior', 'vogal ainda maior'])}'`);

function usandoSortComTS(arr:Array<string>):Array<string>{
    return arr.sort(function (element1:string, element2:string):number{
        return element1.localeCompare(element2);
    });
};
console.log(`Utilizando Sort: ${usandoSortComTS(['aaa','AAaa', 'Fa','Ba','caa', 'Ca','b maior'])}'`);

function usandoReduceComTS(arr:Array<number>):number{
    return arr.reduce(function (total:number, element:number):number{
        return total + element;
    });
};
console.log(`Utilizando Reduce: ${usandoReduceComTS([1,2,3,4,5,6,7,8,9,10])}`);



