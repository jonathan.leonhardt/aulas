let fun: (num1: number, num2: number) => number;
let ext: (valor: number, exponente: number) => number;
/* pode declarar type nome: (num1:number, num2:number) => number; e depois let fun = type*/

function somav2(param1: number, param2: number): number {
    return param1 + param2;
}

function subtrai(param1: number, param2: number): number {
    return param1 - param2;
}

function dividir(param1: number, param2: number): number {
    return param1 / param2;
}

function multiplicar(param1: number, param2: number): number {
    return param1 * param2;
}

function exponecial(param1: number, param2: number): number {
    if (param1 === 0)
        return 0;
    else if ( param2 === 0)
        return 1;
    else {
        let saida: number = param1;
        for (let i = 1; i < param2; i++) {
            saida *= param1;
        }
        return saida;
    }
}


fun = somav2;
console.log(` ${fun(2, 2)} `);
fun = subtrai;
console.log(` ${fun(2, 2)} `);
fun = dividir;
console.log(` ${fun(2, 2)} `);
fun = multiplicar;
console.log(` ${fun(2, 2)} `);

ext = exponecial;
console.log(` ${ext(2, 3)} `);
console.log(` ${ext(0, 5)} `);
console.log(` ${ext(5, 0)} `);