"use strict";
function somhjhjkanv1(param1, param2) {
    return param1 + param2;
}
let somar = somhjhjkanv1;
console.log(`${somar(3, 4)}`);
/* ======================================================================= */
//02
function somarLength(array) {
    return array.join('').length;
}
console.log(somarLength(['ase321klp098', 'wes007', 'klp098', 'ase321', '32']));
function diferencaDatas(parameter1, parameter2) {
    let dt1;
    let dt2;
    let saida = 'Data iguais!';
    if (parameter1 === parameter2) {
        return saida;
    }
    else if (parameter1 > parameter2) {
        dt1 = new Date(parameter2);
        dt2 = new Date(parameter1);
    }
    else {
        dt1 = new Date(parameter1);
        dt2 = new Date(parameter2);
    }
    //no getHours o -1 somente é valido quando hr1>hr2
    let seg = dt2.getSeconds() - dt1.getSeconds();
    let min = dt2.getMinutes() - (seg < 0 ? dt1.getMinutes() - 1 : dt1.getMinutes());
    let hor = dt2.getHours() - (min < 0 ? dt1.getHours() - 1 : dt1.getHours());
    let dia = dt2.getDate() - (hor < 0 ? dt1.getDate() - 1 : dt1.getDate());
    let mes = (dt2.getMonth()) - (dia < 0 ? dt1.getMonth() : dt1.getMonth());
    let ano = dt2.getFullYear() - (mes < 0 ? dt1.getFullYear() - 1 : dt1.getFullYear());
    //return `${ano} anos, ${mes<0?12-mes:mes} meses,${dia<0?(23-dia):(dia+1)} dias,${negSeg(hor)} horas,${negSeg(min)} minutos e ${negSeg(seg)} segundos de diferença!`;
    let anos = ano !== 0 ? ano + ' anos ' : '';
    let meses = mes !== 0 ? (mes < 0 ? 12 - Math.abs(mes) : Math.abs(mes)) + ' meses ' : '';
    let dias = dia !== 0 ? ajustaDias(dia) + ' dias ' : '';
    let horas = hor !== 0 ? (hor < 0 ? 24 - Math.abs(hor) : Math.abs(hor)) + ' horas ' : '';
    let minutos = min !== 0 ? (negSeg(min) + ' minutos ') : '';
    let segundos = seg !== 0 ? (negSeg(seg) + ' segundos ') : '';
    saida = anos + meses + dias + horas + minutos + segundos;
    return saida.concat('de diferença!');
}
function negSeg(param1) {
    return param1 < 0 ? 60 - Math.abs(param1) : Math.abs(param1);
}
/* 18/10/1999 04:34:01
   02/01/2015 23:59:47 */
console.log(diferencaDatas(940228441000, 1420250387000));
console.log(diferencaDatas(1514817241000, 1519925583000));
console.log(diferencaDatas(1519925583000, 1519925583000));
//Aqui que o problema ocorre, como saber de qual mês estamos falando????
function ajustaDias(dias) {
    return dias < 30 ? 30 - Math.abs(dias) : Math.abs(dias);
}
function meuProprioCallback(funCallback, ...parameters) {
    return parameters.reduce(function (element, total) {
        return funCallback(total, element);
    });
}
//testando com outra function de callback
function diminui(param1, param2) {
    return param1 * param2;
}
let calculadora = meuProprioCallback;
console.log(`${calculadora(diminui, 1, 2, 3, 4, 5)}`);
console.log(`${calculadora(somar, 1, 2, 3, 4, 5)}`);
